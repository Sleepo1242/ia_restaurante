/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newpackage;

import java.util.Scanner;

/**
 *
 * @author Emilio
 */
public class logica {
    Scanner scanner = new Scanner(System.in);
    String ramas[] =        {"Clientes", "No", "Si", 
                            "¿TiempoEsperaEstimado?", "No", "¿Alternativa?",
                            "¿Hambriento?", "Si", "¿Reserva?",
                            "¿Vier/Sab?", "Si", "¿Alternativa?",
                            "¿Bar?", "Si", "No", 
                            "Si", "Si", "¿Lloviendo?",
                            "No", "Si", "No", 
                            "Si"};
    String ramas_op[][] = { {"Ninguno", "Algunos", "Lleno", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {">60", "30-60", "10-30", "0-10"},
                            {"", "", "", ""},
                            {"No", "Si", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"No", "Si", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"No", "Si", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            };
    String ramas_val[][] =  {{"No", "Si", "¿TiempoEsperaEstimado?", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"No", "¿Alternativa?", "¿Hambriento?", "Si"},
                            {"", "", "", ""},
                            {"¿Reserva?", "¿Vier/Sab?", "", ""},
                            {"Si", "¿Alternativa?", "", ""},
                            {"", "", "", ""},
                            {"¿Bar?", "Si", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"Sí", "¿Lloviendo?", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"No", "Si", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            {"", "", "", ""},
                            };
    int ramas_pos[][] =  {  {1,2,3,-1},
                            {0,0,0,0},
                            {0,0,0,0},
                            {4,5,6,7},
                            {0,0,0,0},
                            {8,9,-1,-1},
                            {10,11,-1,-1},
                            {0,0,0,0},
                            {12,13,-1,-1},
                            {14,15,-1,-1},
                            {0,0,0,0},
                            {16,17,-1,-1},
                            {18,19,-1,-1},
                            {0,0,0,0},
                            {0,0,0,0},
                            {0,0,0,0},
                            {0,0,0,0},
                            {20,21,-1,-1},
                            {0,0,0,0},
                            {0,0,0,0},
                            {0,0,0,0},
                            {0,0,0,0}
                            };
    String opcion;
    int ini=0;
    
    public void main() {
        System.out.println("");
        //recorrido de cada nodo
        for (int s = 0; s < 22; s++) {
            s=ini;
            System.out.println(ramas[s]);
            //recorrido de cada rama
            for (int b = 0; b < 4; b++) {
                if ("".equals(ramas_op[s][b])) {
                    break;
                }
                System.out.println("Opción " + (b+1) + ": " + ramas_op[s][b]);
            }
                //lee la opción elegida
                System.out.println("\nEscriba correctamente la opción deseada: ");
                opcion = scanner.nextLine();
                //llama al método buscar
                buscar(opcion, s);
            }
        }
    
    public void buscar(String opcion, int x) {
        for (int s=0; s < 4; s++) {
            if (opcion.equals(ramas_op[x][s])) {
                System.out.println("");
                System.out.println("Se tomó la decisión: " + ramas_op[x][s] + " --> " + ramas_val[x][s]);
                if(ramas_pos[ramas_pos[x][s]][s]==0) {
                    switch(ramas[ramas_pos[x][s]]) {
                        case "Si":
                            res_si();
                        case "No":
                            res_no();
                    }
                } else {
                    ini=ramas_pos[x][s];
                }
                break;
            }
        }
}
    public void res_no() {
        System.out.println("Decisión: No se esperará");
        System.exit(0);
    }
    public void res_si() {
        System.out.println("Decisión: Sí se esperará");
        System.exit(0);
    }
}